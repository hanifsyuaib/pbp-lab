# Create your views here.
from django.http.response import HttpResponse
from django.shortcuts import render
from lab_2.models import Note

def index(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab5_index.html', response)

def get_note(request):
	return render(request, 'lab5_index.html', response)

def update_note(request):
	return render(request, 'lab5_index.html', response)
