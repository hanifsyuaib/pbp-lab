from django.urls import path
from .views import index

urlpatterns = [
    path('', index, name='index'),
    path('/notes/<id>', name='notes_id'),
    path('/notes/<id>/update', name='notes_id_update'),
    path('/notes/<id>/delete', name='notes_id_delete'),
]
