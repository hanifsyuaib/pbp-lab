import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_logs/flutter_logs.dart';
//import 'package:path_provider/path_provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'iCovid',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.lightBlue,
      ),
      home: MakeAnnouncementPage(),
    );
  }
}


class MakeAnnouncementPage extends StatefulWidget {
  @override
  _AnnouncementPageState createState() => new _AnnouncementPageState();
}

class _AnnouncementPageState extends State<MakeAnnouncementPage> {
  // For Announcement Form
  final _formKey = GlobalKey<FormState>();
  String _title = "-";
  String _message = "-";

  // For navbar
  int currentIndex = 0;

  // Pop-up announcement button
  createAnnouncementDialog(BuildContext context) {
    return showDialog(context: context, builder: (context) {
      return AlertDialog(
        title: Text("Announcement"),
        content: Text("Title:\n" + _title
            + "\n\nMessage:\n" + _message
        ),
        actions: <Widget>[
          MaterialButton(
            elevation: 5.0,
            child: Text('Ok',
                style: TextStyle(color: Colors.blue),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    });
  }

  AnnouncementForm(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          TextFormField(
            onSaved: (String? value) {
              if (value != null && _formKey.currentState!.validate()) {
                setState(() {
                  _title = value;
                });
              }
            },
            validator: (value) {
              if (value == null || value.isEmpty) {
                return "You can't have an empty title!";
              }
            },
            decoration: const InputDecoration(
                labelText: 'Title',
                helperText: 'Please input the title of the announcement'),
          ),
          TextFormField(
            onSaved: (String? value) {
              if (value != null && _formKey.currentState!.validate()) {
                setState(() {
                  _message = value;
                });
              }
            },
            validator: (value) {
              if (value == null || value.isEmpty) {
                return "You can't have an empty message!";
              }
            },
            decoration: const InputDecoration(
                labelText: 'Message',
                helperText: 'Please input the message of the announcement'),
          ),
          RaisedButton(
            padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0)),
            color: Colors.redAccent,
            textColor: Colors.white,
            child: Text("Post"),
            onPressed: () {
              _formKey.currentState!.save();
              _formKey.currentState!.validate()
                  ? Scaffold.of(context)
                  .showSnackBar(SnackBar(content: Text("Post Success"))
              )
                  : Scaffold.of(context)
                  .showSnackBar(SnackBar(content: Text("This is not valid."))
              );
            },
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Make Announcement'),
      ),
      body: Column(
        children: <Widget>[
          // Announcement button
          FlatButton(
            padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0)),
            color: Colors.redAccent,
            textColor: Colors.white,
            onPressed: () {
              createAnnouncementDialog(context);
            },
            child: Text(
              'Announcement',
              style: TextStyle(fontSize: 20.0),
            ),
          ),
          // end button
          AnnouncementForm(context), // AnnouncementForm
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: currentIndex,
        onTap: (index) {
          setState(() {
            currentIndex = index;
          });
        },
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
            backgroundColor: Colors.lightBlueAccent,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.article_rounded),
            label: 'News',
            backgroundColor: Colors.lightBlueAccent,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.chat),
            label: 'Forum',
            backgroundColor: Colors.lightBlueAccent,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.local_hospital_rounded),
            label: 'Tracker',
            backgroundColor: Colors.lightBlueAccent,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Profile',
            backgroundColor: Colors.lightBlueAccent,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.more_horiz),
            label: 'More',
            backgroundColor: Colors.lightBlueAccent,
          ),
        ],
      ),
    );
  }
}


// IN-PROGRESS
void logFile() {
  var _tag = "MyApp";
  var _myLogFileName = "MyLogFile";
  var toggle = false;
  var logStatus = '';
  //static Completer _completer = new Completer<String>();

  @override
  void initState() {
    //super.initState();
    //setUpLogs();
  }

  void setUpLogs() async {
    await FlutterLogs.initLogs(
        logLevelsEnabled: [
          LogLevel.INFO,
          LogLevel.WARNING,
          LogLevel.ERROR,
          LogLevel.SEVERE
        ],
        timeStampFormat: TimeStampFormat.TIME_FORMAT_READABLE,
        directoryStructure: DirectoryStructure.FOR_DATE,
        logTypesEnabled: [_myLogFileName],
        logFileExtension: LogFileExtension.LOG,
        logsWriteDirectoryName: "MyLogs",
        logsExportDirectoryName: "MyLogs/Exported",
        debugFileOperations: true,
        isDebuggable: true);

    // [IMPORTANT] The first log line must never be called before 'FlutterLogs.initLogs'
    FlutterLogs.logInfo(_tag, "setUpLogs", "setUpLogs: Setting up logs..");

    // Logs Exported Callback
    FlutterLogs.channel.setMethodCallHandler((call) async {
      if (call.method == 'logsExported') {
        // Contains file name of zip
        FlutterLogs.logInfo(
            _tag, "setUpLogs", "logsExported: ${call.arguments.toString()}");

        //setLogsStatus(
        //    status: "logsExported: ${call.arguments.toString()}", append: true);

        // Notify Future with value
        //_completer.complete(call.arguments.toString());
      } else if (call.method == 'logsPrinted') {
        FlutterLogs.logInfo(
            _tag, "setUpLogs", "logsPrinted: ${call.arguments.toString()}");

        //setLogsStatus(
        //    status: "logsPrinted: ${call.arguments.toString()}", append: true);
      }
    });
  }

  void logData({required bool isException}) {
    var logMessage =
        'This is a log message: ${DateTime.now().millisecondsSinceEpoch}';

    if (!isException) {
      FlutterLogs.logThis(
          tag: _tag,
          subTag: 'logData',
          logMessage: logMessage,
          level: LogLevel.INFO);
    } else {
      try {
        if (toggle) {
          toggle = false;
          var i = 100 ~/ 0;
          print("$i");
        } else {
          toggle = true;
          dynamic i;
          print(i * 10);
        }
      } catch (e) {
        if (e is Error) {
          FlutterLogs.logThis(
              tag: _tag,
              subTag: 'Caught an error.',
              logMessage: 'Caught an exception!',
              error: e,
              level: LogLevel.ERROR);
          logMessage = e.stackTrace.toString();
        } else if (e is Exception) {
          FlutterLogs.logThis(
              tag: _tag,
              subTag: 'Caught an exception.',
              logMessage: 'Caught an exception!',
              exception: e,
              level: LogLevel.ERROR);
          logMessage = e.toString();
        }
      }
    }
    //setLogsStatus(status: logMessage);
  }

  void logToFile() {
    var logMessage =
        "This is a log message: ${DateTime.now().millisecondsSinceEpoch}, it will be saved to my log file named: \'$_myLogFileName\'";
    FlutterLogs.logToFile(
        logFileName: _myLogFileName,
        overwrite: false,
        //If set 'true' logger will append instead of overwriting
        logMessage: logMessage,
        appendTimeStamp: true); //Add time stamp at the end of log message
    //setLogsStatus(status: logMessage);
  }

  Future<String> exportAllLogs() async {
    FlutterLogs.exportLogs(exportType: ExportType.ALL);
    //return _completer.future as FutureOr<String>;
    return "";
  }

  void exportFileLogs() {
    FlutterLogs.exportFileLogForName(
        logFileName: _myLogFileName, decryptBeforeExporting: true);
  }

}


