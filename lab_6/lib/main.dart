import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'iCovid',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.lightBlue,
      ),
      home: MakeAnnouncementPage(),
    );
  }
}

class MakeAnnouncementPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Make Announcement'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: AnnouncementForm(),
      ),
    );
  }

}

class AnnouncementForm extends StatefulWidget {
  @override
  _AnnouncementFormState createState() => new _AnnouncementFormState();
}

class _AnnouncementFormState extends State<AnnouncementForm> {

  final _formKey = GlobalKey<FormState>();

  String _title = "-";
  String _message = "-";

  // Pop-up announcement
  createAnnouncementDialog(BuildContext context) {
    return showDialog(context: context, builder: (context){
      return AlertDialog(
        title: Text("Announcement"),
        content: Text("Title:\n" + _title
            + "\n\nMessage:\n" + _message
        ),
        actions: <Widget>[
          MaterialButton(
            elevation: 5.0,
            child: Text('Ok'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            // Announcement button
            FlatButton(
              padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0)),
              color: Colors.redAccent,
              textColor: Colors.white,
              onPressed: () {
                createAnnouncementDialog(context);
              },
              child: Text(
                'Announcement',
                style: TextStyle(fontSize: 20.0),
              ),
            ),
            // end button
            TextFormField(
              onSaved: (String? value) {
                if (value != null) {
                  setState(() {
                    _title = value;
                  });
                }
              },
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return "You can't have an empty title!";
                }
              },
              decoration: const InputDecoration(
                labelText: 'Title',
                helperText: 'Please input the title of the announcement'),
            ),
            TextFormField(
              onSaved: (String? value) {
                if (value != null) {
                  setState(() {
                    _message = value;
                  });
                }
              },
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return "You can't have an empty message!";
                }
              },
              decoration: const InputDecoration(
                  labelText: 'Message',
                  helperText: 'Please input the message of the announcement'),
            ),
            RaisedButton(
              child: Text("Post"),
              onPressed: () {
                _formKey.currentState!.save();
                _formKey.currentState!.validate()
                    ? Scaffold.of(context)
                      .showSnackBar(SnackBar(content: Text("Post Success"))
                      )
                    : Scaffold.of(context)
                      .showSnackBar(SnackBar(content: Text("This is not valid."))
                      );
              },
            ),
          ],
        ),
    );
  }
}


