from django.forms import ModelForm
from lab_1.models import Friend as FriendModel

class FriendForm(ModelForm):
	class Meta:
		model = FriendModel
		fields = ['name','npm','DOB']
		