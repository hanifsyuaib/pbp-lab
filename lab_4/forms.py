from django.forms import ModelForm
from django import forms
from lab_2.models import Note as NoteModel

class NoteForm(ModelForm):
	class Meta:
		model = NoteModel
		fields = ['to','sender','title','message']
		
		