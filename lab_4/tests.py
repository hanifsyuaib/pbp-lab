from django.test import TestCase
# Create your tests here.
from django.test import Client
from django.urls import resolve
from .views import index, add_note, note_list
from django.http import HttpRequest


class Lab4UnitTest(TestCase):

    def test_index_is_exist(self):
        response = Client().get('/lab-4/')
        self.assertEqual(response.status_code, 200)    

    def test_using_index_func(self): 
        found = resolve('/lab-4/')
        self.assertEqual(found.func, index)

    def test_add_note_is_exist(self):
        response = Client().get('/lab-4/add-note')
        self.assertEqual(response.status_code, 200)    

    def test_using_add_note_func(self): 
        found = resolve('/lab-4/add-note')
        self.assertEqual(found.func, add_note)

    def test_note_list_is_exist(self):
        response = Client().get('/lab-4/note-list')
        self.assertEqual(response.status_code, 200)    

    def test_using_note_list_func(self): 
        found = resolve('/lab-4/note-list')
        self.assertEqual(found.func, note_list)    

     


