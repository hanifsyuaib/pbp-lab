## Lab 2: Pertanyaan singkat

**1. Apakah perbedaan antara JSON dan XML?**
   1. JSON
      - Memiliki data interchange file format
      - Berupa data oriented
      - Data siap untuk diakses
      - Mengambil data/value mudah
      - Tidak mampu melakukan display data
   
   2. XML
      - Memiliki markup language file format
      - Berupa document oriented
      - Data perlu di-parsed terlebih dahulu
      - Mengambil data/value sulit
      - Mampu men-display data karena markup language
  
**2. Apakah perbedaan antara HTML dan XML?**
   1. HTML
      - Dipakai untuk menampilkan data
      - Tags penutup tidak diwajibkan
      - Tidak case sensitive
      - Hanya memakai tags yang ada
      - Static karena menampilkan data
   
   2. XML
      - Dipakai untuk menyimpan dan mengirim data dari atau ke database
      - Tags penutup wajib ada
      - Case sensitive
      - Bisa membuat tags
      - Dinamis karena untuk mengirimkan data
   

Referensi:<br>
JSON vs XML. https://www.educba.com/json-vs-xml/<br>
JSON vs XML: What’s the Difference?. https://www.guru99.com/json-vs-xml-difference.html<br>
HTML Vs XML: Difference Between HTML and XML [2021]. https://www.upgrad.com/blog/html-vs-xml/<br>
HTML vs XML. https://www.javatpoint.com/html-vs-xml<br>
